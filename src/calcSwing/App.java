package calcSwing;

import java.awt.EventQueue;

import calcSwing.view.CalcView;


public class App {
	
	public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalcView window = new CalcView();
					Controller controller = new Controller(window);
					window.initialize(controller);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
