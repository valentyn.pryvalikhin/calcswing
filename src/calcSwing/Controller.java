package calcSwing;

import calcSwing.model.CalcModel;
import calcSwing.view.CalcView;

public class Controller {
	
	
  
	CalcView calcView;
	CalcModel model = new CalcModel();
	
	private String history = "";
			
	
	public Controller(CalcView calcView) {
		super();
		this.calcView = calcView;
	}

	public String[] getOperations() {
		Object[] arr = model.getCalcMap().keySet().toArray();
		String[] arrOperationsStr = new String[arr.length];
		
		for (int i = 0; i < arr.length; i++) {
			arrOperationsStr[i] = (String) arr[i];	
		}
		return arrOperationsStr;
	}
	
	public void makeResultAndAddToHistory (double numA,double numB,String operator) {
		
		double result = model.getCalcMap().get(operator).operate(numA, numB);				
		String historyString = numA + " " + operator + " " + numB + " = " + result + "\n";
		history += historyString;	
		
		calcView.getTextResultOutput().setText(Double.toString(result));
		calcView.getHistoryText().setText(history);
		
	}
	
	
}
