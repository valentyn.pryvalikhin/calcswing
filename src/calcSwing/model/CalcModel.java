package calcSwing.model;

import java.util.LinkedHashMap;

import java.util.Map;

import calcSwing.model.operations.Operation;
import calcSwing.model.operations.OperationAddition;
import calcSwing.model.operations.OperationDivision;
import calcSwing.model.operations.OperationMultiplication;
import calcSwing.model.operations.OperationSubstraction;

public class CalcModel {
	
	
	
	public CalcModel() {
		
		initCalcMap();
	}

	private Map<String, Operation> calcMap = new LinkedHashMap<String , Operation>();
	  	
	private void initCalcMap() {
		calcMap.put("+", new OperationAddition());
		calcMap.put("-", new OperationSubstraction());
		calcMap.put("*", new OperationMultiplication());
		calcMap.put("/", new OperationDivision());
	}

	public Map<String, Operation> getCalcMap() {
		Map<String, Operation> calcMapClone = new LinkedHashMap<String , Operation>(calcMap);
		return calcMapClone;
	}
}
