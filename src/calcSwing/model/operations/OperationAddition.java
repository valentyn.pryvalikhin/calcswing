package calcSwing.model.operations;

public class OperationAddition implements Operation {

	@Override
	public double operate(double numA, double numB) {
		
		return numA + numB;
	}


}
