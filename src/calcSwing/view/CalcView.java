package calcSwing.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import calcSwing.Controller;

public class CalcView {
	
	private final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private JTextField textInputA;
	private JTextField textInputB;
	private JComboBox comboOperation;
	private JTextField textResultOutput;
	private JButton btnCalculate;
	private JTextPane historyText;
	private JFrame frame;
	private boolean onFly = true;
		
	public void initialize(Controller controller) {
				
		frame = new JFrame();
		frame.setVisible(true);
		frame.setBounds(100, 100, 300, 330);
		frame.setMinimumSize(new Dimension(300, 220));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		frame.getContentPane().add(tabbedPane);
		
		JPanel panelCalc = new JPanel();
		tabbedPane.addTab("Calculator", null, panelCalc, null);
		panelCalc.setLayout(new BoxLayout(panelCalc, BoxLayout.Y_AXIS));
		
		JPanel panelInput = new JPanel();
		panelCalc.add(panelInput);
		panelInput.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		textInputA = new JTextField();
		panelInput.add(textInputA);
		textInputA.setColumns(10);
		textInputA.setText("0");
		
		addTextInputListeners(textInputA, controller);
		
		comboOperation = new JComboBox(controller.getOperations());
		
		comboOperation.addItemListener( event -> {
				
				if (onFly) {
					countAndAddHistory(controller);
					}
			}
		);
		
		panelInput.add(comboOperation);
		
		textInputB = new JTextField();
		textInputB.setColumns(10);
		textInputB.setText("0");
		panelInput.add(textInputB);
		
		addTextInputListeners(textInputB, controller);
		
		JPanel panelOutput = new JPanel();
		panelCalc.add(panelOutput);
		
		JCheckBox chkBoxOnFly = new JCheckBox("Calculate on the fly");
		chkBoxOnFly.setSelected(true);
		
		chkBoxOnFly.addItemListener( event -> {
				
				if (((JCheckBox) event.getSource()).isSelected()) {
					onFly = true;		
					btnCalculate.setEnabled(false);
					countAndAddHistory(controller);
				} else {
					onFly = false;
					btnCalculate.setEnabled(true);
				}
			}
		);
		
		btnCalculate = new JButton("Calculate");
		btnCalculate.setEnabled(false);
		
		btnCalculate.addActionListener( 
				event -> countAndAddHistory(controller)
				);
		
		textResultOutput = new JTextField();
		textResultOutput.setColumns(10);
		textResultOutput.setText("0.0");
		
		GroupLayout gl_panelOutput = new GroupLayout(panelOutput);
		gl_panelOutput.setHorizontalGroup(
			gl_panelOutput.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panelOutput.createSequentialGroup()
					.addGroup(gl_panelOutput.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panelOutput.createSequentialGroup()
							.addContainerGap()
							.addComponent(textResultOutput, GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE))
						.addGroup(gl_panelOutput.createSequentialGroup()
							.addGap(6)
							.addComponent(chkBoxOnFly)
							.addPreferredGap(ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
							.addComponent(btnCalculate)))
					.addContainerGap())
		);
		
		gl_panelOutput.setVerticalGroup(
			gl_panelOutput.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelOutput.createSequentialGroup()
					.addGap(56)
					.addGroup(gl_panelOutput.createParallelGroup(Alignment.BASELINE)
						.addComponent(chkBoxOnFly)
						.addComponent(btnCalculate))
					.addGap(7)
					.addComponent(textResultOutput, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		panelOutput.setLayout(gl_panelOutput);
		
		JPanel panelHistory = new JPanel();
		tabbedPane.addTab("History", null, panelHistory, null);
		panelHistory.setLayout(new BoxLayout(panelHistory, BoxLayout.X_AXIS));
		
		historyText = new JTextPane();
		
		JScrollPane scrollPane = new JScrollPane(historyText);
		panelHistory.add(scrollPane);
	}
	
	private void countAndAddHistory (Controller controller) {
			
			double numA = Double.parseDouble(InputCorrector.correct(textInputA.getText()));
			double numB = Double.parseDouble(InputCorrector.correct(textInputB.getText()));
			String operator = (String) comboOperation.getSelectedItem();
			
			controller.makeResultAndAddToHistory(numA, numB, operator);				
	}
	
	private void addTextInputListeners(JTextField textInput, Controller controller) {
		textInput.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				
				if (DigitInputVerifier.verify(e)) {
					e.consume();
				}}
		});
		
		textInput.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (onFly) {
					countAndAddHistory(controller);
					}
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (onFly) {
					countAndAddHistory(controller);
					}
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				if (onFly) {
					countAndAddHistory(controller);
					}
			}
		});
	}


	public JTextField getTextResultOutput() {
		return textResultOutput;
	}



	public JTextPane getHistoryText() {
		return historyText;
	}
	
	
}
