package calcSwing.view;

import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;

public class DigitInputVerifier {

	
	public static boolean verify(KeyEvent e) {
		
		
		char newChar = e.getKeyChar();
		StringBuffer oldText = new StringBuffer(((JTextField) e.getSource()).getText());
		
		oldText = oldText.insert(((JTextField)e.getSource()).getSelectionStart(), newChar);
		String strForTest = oldText.toString();
		Pattern p = Pattern.compile("([+-]? *(?:\\d+(?:\\.\\d*)?|\\.\\d+)?)|(^[+-])");
		Matcher m = p.matcher(strForTest);
		
		if (!m.matches()) {
			return true;
		} else
		return false;
	}

}
