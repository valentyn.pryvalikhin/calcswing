package calcSwing.view;

public class InputCorrector {
	
	public static String correct(String input) {
		
		switch (input) {
		case "":
			return "0";
		case "+":
			return "+0";
		case "-":
			return "-0";
		default:
			return input;
		}
	}

}
